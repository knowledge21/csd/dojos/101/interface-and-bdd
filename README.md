# Dojo de BDD com Ruby e Cucumber

Este Dojo foi feito com base no artigo original do Caique Coelho:

https://medium.com/@caiquecoelho/testando-com-bdd-cucumber-capybara-ruby-e-rails-p-8c9018e05463

Obrigado, Caique!

## Instalação

Para executá-lo você precisa:

- Instalar o [Ruby](https://www.ruby-lang.org/pt/documentation/installation/)

- Instalar o [Bundle](https://bundler.io/) (é só rodar o comando abaixo depois de instalar o Ruby):

```sh
gem install bundler
```

## Execução

Para executar, basta estar no diretório raíz e digitar primeiro a linha de comando no diretório do projeto. Ela vai instalar todas as dependências necessárias:

```sh
bundle install
```

Depois é só mandar este comando abaixo para executar os testes com o cucumber:

```sh
cucumber
```

;)

Qualquer dúvida por me chamar no [Twitter](https://twitter.com/luizphx)