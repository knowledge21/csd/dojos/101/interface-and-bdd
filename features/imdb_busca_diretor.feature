#language: pt

Funcionalidade: Busca pelo nome do diretor

    COMO um cinefilo 
    EU QUERO acessar a página de um filme no imdb 
    AFIM de ver quem dirigiu 
    
    Cenário: Busca o Kill Bill e encontra o grande Quentin Tarantino (com risco de ter sangue espalhado ne)
        Dado que eu acessei o imdb
        Quando eu buscar por "Kill Bill"
        E entrar na pagina do filme
        Então o "Quentin Tarantino" deve ser exibido como diretor